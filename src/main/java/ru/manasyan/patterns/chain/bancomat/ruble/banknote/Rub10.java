package ru.manasyan.patterns.chain.bancomat.ruble.banknote;

import ru.manasyan.patterns.chain.bancomat.banknote.Banknote;
import ru.manasyan.patterns.chain.bancomat.banknote.CurrencyType;

public class Rub10 extends Banknote {
    public Rub10() {
        super(CurrencyType.RUB, 10L);
    }
}
