package ru.manasyan.patterns.chain.bancomat.dollar;

import ru.manasyan.patterns.chain.bancomat.banknote.Banknote;
import ru.manasyan.patterns.chain.bancomat.banknote.BanknoteHandler;
import ru.manasyan.patterns.chain.bancomat.banknote.CurrencyType;
import ru.manasyan.patterns.chain.bancomat.dollar.banknote.Dollar100;

public class HundredDollarHandler extends DollarHandlerBase {

    protected Banknote banknote = new Dollar100();

    public HundredDollarHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected long getValue() {
        return banknote.getValue();
    }

    @Override
    protected CurrencyType getCurrency() {
        return banknote.getCurrency();
    }
}
