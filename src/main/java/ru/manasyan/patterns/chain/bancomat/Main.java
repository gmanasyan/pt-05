package ru.manasyan.patterns.chain.bancomat;

import ru.manasyan.patterns.chain.bancomat.banknote.CurrencyType;

/**
 * Main.
 *
 * @author Ilya_Sukhachev
 */
public class Main {

    public static void main(String[] args) {
        Bancomat bancomat = new Bancomat();
        bancomat.getMoney( "370", CurrencyType.USD);
        bancomat.getMoney( "452", CurrencyType.USD);
        bancomat.getMoney( "1420", CurrencyType.RUB);
        bancomat.getMoney( "255", CurrencyType.EUR);
    }
}
