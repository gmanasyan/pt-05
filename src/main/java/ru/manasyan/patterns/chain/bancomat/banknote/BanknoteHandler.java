package ru.manasyan.patterns.chain.bancomat.banknote;

/**
 * BanknoteHandler.
 *
 * @author Ilya_Sukhachev
 */
public abstract class BanknoteHandler {

    private BanknoteHandler nextHandler;

    protected BanknoteHandler(BanknoteHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public boolean validate(Long amount) {
        return nextHandler != null && nextHandler.validate(amount);
    }

    public boolean giveMeCash(Long amount) {
        return nextHandler != null && nextHandler.giveMeCash(amount);
    }
}
