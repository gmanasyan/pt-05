package ru.manasyan.patterns.chain.bancomat.ruble;

import ru.manasyan.patterns.chain.bancomat.banknote.BanknoteHandler;
import ru.manasyan.patterns.chain.bancomat.banknote.CurrencyType;

public abstract class RubHandlerBase extends BanknoteHandler {
    protected RubHandlerBase(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    public boolean validate(Long amount) {
        if (amount % getValue() == 0) {
            return true;
        }
        return super.validate(amount % getValue());
    }

    @Override
    public boolean giveMeCash(Long amount) {
        long banknotes = amount / getValue();
        if (banknotes != 0) {
            System.out.println("Grab your: " + banknotes + " * "
                    + getValue() + " " + getCurrency());
        }
        if (banknotes != 0 && amount % getValue() == 0) {
            return true;
        }
        return super.giveMeCash(amount % getValue());
    }

    protected abstract long getValue();

    protected abstract CurrencyType getCurrency();

}
