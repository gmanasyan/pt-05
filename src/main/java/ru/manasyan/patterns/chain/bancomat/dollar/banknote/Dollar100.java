package ru.manasyan.patterns.chain.bancomat.dollar.banknote;

import ru.manasyan.patterns.chain.bancomat.banknote.Banknote;
import ru.manasyan.patterns.chain.bancomat.banknote.CurrencyType;

public class Dollar100 extends Banknote {
    public Dollar100() {
        super(CurrencyType.USD, 100L);
    }
}
