package ru.manasyan.patterns.chain.bancomat.ruble.banknote;

import ru.manasyan.patterns.chain.bancomat.banknote.Banknote;
import ru.manasyan.patterns.chain.bancomat.banknote.CurrencyType;

public class Rub1000 extends Banknote {
    public Rub1000() {
        super(CurrencyType.RUB, 1000L);
    }
}
