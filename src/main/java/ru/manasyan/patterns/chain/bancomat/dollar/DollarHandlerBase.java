package ru.manasyan.patterns.chain.bancomat.dollar;

import ru.manasyan.patterns.chain.bancomat.banknote.BanknoteHandler;
import ru.manasyan.patterns.chain.bancomat.banknote.CurrencyType;

public abstract class DollarHandlerBase extends BanknoteHandler {
    protected DollarHandlerBase(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    public boolean validate(Long amount) {
        if (amount % getValue() == 0) {
            return true;
        }
        return super.validate(amount % getValue());
    }

    @Override
    public boolean giveMeCash(Long amount) {
        System.out.println("Grab your: " + amount / getValue() + " * "
                + getValue() + " " + getCurrency());
        if (amount % getValue() == 0) {
            return true;
        }
        return super.giveMeCash(amount % getValue());
    }

    protected abstract long getValue();

    protected abstract CurrencyType getCurrency();

}
