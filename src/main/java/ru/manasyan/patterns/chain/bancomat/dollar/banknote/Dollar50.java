package ru.manasyan.patterns.chain.bancomat.dollar.banknote;

import ru.manasyan.patterns.chain.bancomat.banknote.Banknote;
import ru.manasyan.patterns.chain.bancomat.banknote.CurrencyType;

public class Dollar50 extends Banknote {
    public Dollar50() {
        super(CurrencyType.USD, 50L);
    }
}
