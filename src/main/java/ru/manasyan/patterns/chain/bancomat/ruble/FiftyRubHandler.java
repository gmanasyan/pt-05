package ru.manasyan.patterns.chain.bancomat.ruble;

import ru.manasyan.patterns.chain.bancomat.banknote.Banknote;
import ru.manasyan.patterns.chain.bancomat.banknote.BanknoteHandler;
import ru.manasyan.patterns.chain.bancomat.banknote.CurrencyType;
import ru.manasyan.patterns.chain.bancomat.ruble.banknote.Rub50;

public class FiftyRubHandler extends RubHandlerBase {

    protected Banknote banknote = new Rub50();

    public FiftyRubHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected long getValue() {
        return banknote.getValue();
    }

    @Override
    protected CurrencyType getCurrency() {
        return banknote.getCurrency();
    }
}
