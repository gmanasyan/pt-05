package ru.manasyan.patterns.chain.bancomat.ruble;

import ru.manasyan.patterns.chain.bancomat.banknote.Banknote;
import ru.manasyan.patterns.chain.bancomat.banknote.BanknoteHandler;
import ru.manasyan.patterns.chain.bancomat.banknote.CurrencyType;
import ru.manasyan.patterns.chain.bancomat.ruble.banknote.Rub1000;

public class ThousandRubHandler extends RubHandlerBase {

    protected Banknote banknote = new Rub1000();

    public ThousandRubHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected long getValue() {
        return banknote.getValue();
    }

    @Override
    protected CurrencyType getCurrency() {
        return banknote.getCurrency();
    }
}
