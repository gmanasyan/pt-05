package ru.manasyan.patterns.chain.bancomat.ruble;

import ru.manasyan.patterns.chain.bancomat.banknote.Banknote;
import ru.manasyan.patterns.chain.bancomat.banknote.BanknoteHandler;
import ru.manasyan.patterns.chain.bancomat.banknote.CurrencyType;
import ru.manasyan.patterns.chain.bancomat.ruble.banknote.Rub100;

public class HundredRubHandler extends RubHandlerBase {

    protected Banknote banknote = new Rub100();

    public HundredRubHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected long getValue() {
        return banknote.getValue();
    }

    @Override
    protected CurrencyType getCurrency() {
        return banknote.getCurrency();
    }
}
