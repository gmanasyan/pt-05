package ru.manasyan.patterns.chain.bancomat.dollar.banknote;

import ru.manasyan.patterns.chain.bancomat.banknote.Banknote;
import ru.manasyan.patterns.chain.bancomat.banknote.CurrencyType;

public class Dollar10 extends Banknote {
    public Dollar10() {
        super(CurrencyType.USD, 10L);
    }
}
