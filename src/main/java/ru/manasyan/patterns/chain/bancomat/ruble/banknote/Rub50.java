package ru.manasyan.patterns.chain.bancomat.ruble.banknote;

import ru.manasyan.patterns.chain.bancomat.banknote.Banknote;
import ru.manasyan.patterns.chain.bancomat.banknote.CurrencyType;

public class Rub50 extends Banknote {
    public Rub50() {
        super(CurrencyType.RUB, 50L);
    }
}
