package ru.manasyan.patterns.chain.bancomat;

import ru.manasyan.patterns.chain.bancomat.banknote.BanknoteHandler;
import ru.manasyan.patterns.chain.bancomat.banknote.CurrencyType;
import ru.manasyan.patterns.chain.bancomat.dollar.FiftyDollarHandler;
import ru.manasyan.patterns.chain.bancomat.dollar.HundredDollarHandler;
import ru.manasyan.patterns.chain.bancomat.dollar.TenDollarHandler;
import ru.manasyan.patterns.chain.bancomat.ruble.FiftyRubHandler;
import ru.manasyan.patterns.chain.bancomat.ruble.HundredRubHandler;
import ru.manasyan.patterns.chain.bancomat.ruble.TenRubHandler;
import ru.manasyan.patterns.chain.bancomat.ruble.ThousandRubHandler;

/**
 * Bancomat.
 *
 * @author Ilya_Sukhachev
 */
public class Bancomat {
    private BanknoteHandler handlerDollar;
    private BanknoteHandler handlerRub;

    public Bancomat() {
        handlerDollar = new TenDollarHandler(null);
        handlerDollar = new FiftyDollarHandler(handlerDollar);
        handlerDollar = new HundredDollarHandler(handlerDollar);

        handlerRub= new TenRubHandler(null);
        handlerRub = new FiftyRubHandler(handlerRub);
        handlerRub = new HundredRubHandler(handlerRub);
        handlerRub = new ThousandRubHandler(handlerRub);
    }

    public boolean getMoney(String amount, CurrencyType currencyType) {
        System.out.println("------------------");
        System.out.println("Sberbank ATM 3452");
        System.out.println("Get " + amount + " " + currencyType.name());

        BanknoteHandler handler;
        switch (currencyType) {
            case USD:
                handler = handlerDollar;
                break;
            case RUB:
                handler = handlerRub;
                break;
            default:
                System.out.println("Sorry, no such currency.");
                return false;
        }

        boolean validAmount = handler.validate(Long.valueOf(amount));
        if (validAmount) {
            handler.giveMeCash(Long.valueOf(amount));
            return true;
        } else {
            System.out.println("Sorry, the requested amount can't be given.");
            return false;
        }
    }

}
