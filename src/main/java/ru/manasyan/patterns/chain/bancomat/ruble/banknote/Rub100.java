package ru.manasyan.patterns.chain.bancomat.ruble.banknote;

import ru.manasyan.patterns.chain.bancomat.banknote.Banknote;
import ru.manasyan.patterns.chain.bancomat.banknote.CurrencyType;

public class Rub100 extends Banknote {
    public Rub100() {
        super(CurrencyType.RUB, 100L);
    }
}
