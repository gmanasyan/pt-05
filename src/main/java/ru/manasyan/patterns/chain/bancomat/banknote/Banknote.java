package ru.manasyan.patterns.chain.bancomat.banknote;

/**
 * Banknote.
 *
 * @author Ilya_Sukhachev
 */
public abstract class Banknote {

    protected CurrencyType currency;
    protected Long value;

    public Banknote(CurrencyType currency, Long value) {
        this.currency = currency;
        this.value = value;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public Long getValue() {
        return value;
    }
}
